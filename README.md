Steps to run the project:

1. Clone the repo
2. cd to "src/test/resources"
3. cp settings.properties.sample to settings.properties
4. Update the values in settings.properties
5. cd to REPO_HOME
6. On linux run ./gradlew test. On windows run gradle.bat test