import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

/**
 * Created by chaitu on 2/4/18.
 */
public class TestLogin extends BaseTestCase {
    private static final String URL = "https://www.techgig.com/";
    private static final String SETTINGS_FILE_PATH = "settings.properties";
    private static final String USER_NAME;
    private static final String PASSWORD;

    static {
        Properties prop = new Properties();
        try {
            prop.load(TestLogin.class.getResourceAsStream(SETTINGS_FILE_PATH));
        } catch (IOException e) {
            System.err.println("Error reading from settings file: " + SETTINGS_FILE_PATH);
            e.printStackTrace();
            System.exit(1);
        }
        USER_NAME = prop.getProperty("site.username");
        PASSWORD = prop.getProperty("site.password");
    }

    /**
     * User Story:  As a techgig user I should be able to login
     *
     * Steps:
     *   1. Navigate to techgig website
     *   2. Click on Login link
     *   3. Type in username and password
     *   4. Click on "Login" button
     */
    @Test
    public void testLogin() {
        WebDriver driver = getWebDriver(Drivers.CHROME);
        driver.get(URL);
        List<WebElement> elements = driver.findElements(By.cssSelector(".login-signup-links a"));
        WebElement loginLink = elements.get(0);
        Assert.assertEquals("Verify you got login link", loginLink.getText(), "Login");

        loginLink.click();
        System.out.println(driver.getCurrentUrl());
        WebElement element = driver.findElement(By.id("username"));
        element.sendKeys(USER_NAME);
        element = driver.findElement(By.id("password"));
        element.sendKeys(PASSWORD);

        element = driver.findElement(By.id("button_login"));
        element.click();
    }
}
